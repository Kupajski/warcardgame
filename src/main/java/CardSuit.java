/**
 * @author Bartosz Kupajski
 */
//TODO enum may be not public
public enum CardSuit {

    HEART(),
    SPADES(),
    CLUB(),
    DIAMOND(),

}
