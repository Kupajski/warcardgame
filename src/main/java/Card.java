/**
 * @author Bartosz Kupajski
 */


 class Card {

    private CardSuit suit;
    private CardValues values;

    Card(CardSuit suit, CardValues values) {
        this.suit = suit;
        this.values = values;
    }

    Card(CardValues values) {
        this.values = values;
    }

    CardValues getValues() {
        return values;
    }


}
