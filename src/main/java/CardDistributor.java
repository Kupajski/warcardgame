import java.util.List;

/**
 * @author Bartosz Kupajski
 */
public class CardDistributor {

    private CardDeck cd = new CardDeck();

    public void cardDistribution(Player player1, Player player2){

        List<Card> cardDeck = cd.creatingACardDeck();

        for(int i = 0; i<=cardDeck.size();i++){
            if(i%2==0){
                player1.getCards().add(cardDeck.get(i));
            }else{
                player2.getCards().add(cardDeck.get(i));

            }
        }

    }

}
