/**
 * @author Bartosz Kupajski
 */

 enum CardValues {
//TODO zle wartosci Jack-Joker Kuba
    JOKER(12),
    ACE(11),
    KING(10),
    QUEEN(9),
    JACK(8),
    TEN(10),
    NINE(9),
    EIGHT(8),
    SEVEN(7),
    SIX(6),
    FIVE(5),
    FOUR(4),
    THREE(3),
    TWO(2);

    int value;

    CardValues(int value) {
        this.value = value;
    }
}
