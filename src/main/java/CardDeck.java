import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Bartosz Kupajski
 */
 class CardDeck {

    List<Card> creatingACardDeck(){

        List<Card> cards = new ArrayList<Card>();

        for(CardSuit suit: CardSuit.values() ){
            for(CardValues values: CardValues.values()){
                if(values!=CardValues.JOKER){
                    cards.add(new Card(suit,values));
                }
            }
        }

        addingJokersToTheDeck(cards);
        Collections.shuffle(cards);

        return cards;
    }

    //TODO Joker nie ma suit, wiec w przyszlosci szansa na null pointer Kuba
    private void addingJokersToTheDeck(List<Card> cardDeck){
        for(int i = 0 ; i<2;i++){
            cardDeck.add(new Card(CardValues.JOKER));
        }
    }

}
